mod comm_vec;
mod info;
mod device;
mod display;

use anyhow::Error;
use pbr::{ProgressBar, Units};
use std::{
    thread,
    time::Duration,
};
use argh::FromArgs;
use comm::*;
use image::*;
use info::print_error;
use device::*;
use display::display_buffer;

const CHIP_SIZE: usize = 2048;


#[derive(FromArgs)]
/// Args
struct Cli {
    /// image
    #[argh(option, short = 'i')]
    image: Option<String>,

    /// programmer
    #[argh(option, short = 'p')]
    programmer: Option<String>,

    /// display
    #[argh(switch, short = 'd')]
    display: bool,

    /// offset
    #[argh(option, short = 'o', default = "0x0000")]
    offset: usize,

    /// length
    #[argh(option, short = 'l', default = "2048")]
    length: usize,
}

impl Cli {
    fn run(self) -> Result<(), Error> {
        let programmer = if let Some(programmer) = self.programmer {
            Ok(programmer)
        } else {
            let ports = serialport::available_ports()?;
            println!("Recognized serial ports:");
            for port in ports {
                println!("  {}: {:?}", port.port_name, port.port_type);
            }
            Err(Error::msg("Programmer not given"))
        }?;

        if let Some(path) = &self.image {
            if !std::path::Path::new(path).exists() {
                return Err(Error::msg(format!("Image file `{}` does not exist", path)));
            }
        }

        let port = serialport::new(&programmer, 115200).open().map_err(|err| {
            Error::new(err).context(format!("Failed to connect to programmer `{}`", programmer))
        })?;
        let mut reader = std::io::BufReader::new(port);

        task_message!("Waiting", "Programmer is starting...");

        // programmer takes some time to restart
        wait_for_startup(&mut reader)?;

        // ping the programmer
        match device_request(&Command::Ping(100), &mut reader)? {
            Response::Ping(101) => Ok(()),
            _ => Err(Error::msg("Failed to connect to programmer")),
        }?;

        task_message!("Ready", "Programmer is ready");

        if let Some(path) = &self.image {
            task_message!("Programming", "Programming segements in image...");

            // load image
            let json = std::fs::read_to_string(&path)
                .map_err(|err| Error::new(err).context(format!("Failed to read image `{}`", path)))?;
            let image = serde_json::from_str::<Segments>(&json)
                .map_err(|err| Error::new(err).context(format!("Failed to parse image `{}`", path)))?;

            for segment in &image {
                let length = segment.data.len();
                let offset = segment.offset;

                println!();
                println!("=== {} ===", segment.label);
                println!("Offset: {:#06X}", offset);
                println!("Length: {} bytes", length);

                if offset + length >= CHIP_SIZE {
                    return Err(Error::msg("Segment data does not fit on chip"));
                }

                let mut pb = ProgressBar::new(length as u64);
                pb.message("Writing: ");
                pb.set_units(Units::Bytes);
                pb.set(0);
                for (index, byte) in segment.data.iter().enumerate() {
                    let address = (index + offset) as u16;
                    match device_request(
                        &Command::Write {
                            address,
                            data: *byte,
                        },
                        &mut reader,
                    )? {
                        Response::Write => {}
                        _ => {
                            return Err(Error::msg(format!(
                                "Failed to write to address {:#06X}",
                                address
                            )))
                        }
                    }
                    pb.inc();
                }
                pb.finish_println("");

                // verify

                thread::sleep(Duration::from_secs_f64(0.1));
                
                let mut pb = ProgressBar::new(length as u64);
                pb.message("Verifying: ");
                pb.set_units(Units::Bytes);
                pb.set(0);
                for (index, byte) in segment.data.iter().enumerate() {
                    let address = (index + offset) as u16;
                    match device_request(&Command::Read { address }, &mut reader)? {
                        Response::Read{data} => {
                            if *byte == data {
                                pb.inc();
                            } else {
                                return Err(Error::msg(format!("Verification failed, byte at {:#06X} is {:#04X} suppose to be {:#04X}", address, data, byte)));
                            }
                        },
                        _ => return Err(Error::msg("Invalid read response")),
                    }
                    pb.inc();
                }
                pb.finish_println("");
            }
        }

        if self.display {
            // dump chip
            task_message!("Display", "Reading chip's data...");
            let mut buffer = Vec::new();

            let mut pb = ProgressBar::new(self.length as u64);
            pb.message("Reading: ");
            pb.set_units(Units::Bytes);
            pb.set(0);
            let args_offset = self.offset;
            for i in (0..self.length).map(|x| x + args_offset).filter(|x| x < &CHIP_SIZE) {
                match device_request(&Command::Read { address: i as u16 }, &mut reader)? {
                    Response::Read{data} => {
                        buffer.push(data);
                        pb.inc();
                    },
                    _ => return Err(Error::msg("Invalid read response")),
                }
            }
            pb.finish_println("");

            task_message!("Done", "Finished reading data");

            println!();
            display_buffer(buffer, self.offset);
            println!();
        }

        return Ok(());
    }
}

fn main() {
    let cli: Cli = argh::from_env();
    match cli.run() {
        Ok(_) => {}
        Err(err) => {
            print_error(err);
            std::process::exit(1);
        }
    }
}
