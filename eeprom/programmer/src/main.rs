#![no_std]
#![no_main]
#![feature(array_map)]

mod packet;
mod shift;

use arduino_hal::{
    hal::port::*,
    port::mode::{Floating, Input, Output},
};
use comm::*;
use packet::{ReadPacket, WritePacket};
use shift::ShiftRegister;

use panic_halt as _;

fn startup_packet() -> Packet {
    Packet {
        id: 42,
        data: Data::Response(Response::Ping(101)),
    }
}

fn process_ping(id: u8, value: u8) -> Packet {
    Packet {
        id,
        data: Data::Response(Response::Ping(value.wrapping_add(1))),
    }
}

fn process_read(
    id: u8,
    address: u16,
    shift_reg: &mut ShiftRegister<PD2, PD3, PD4>,
    data_reg: &DataRegister<Input<Floating>>,
) -> Packet {
    // output 11-bit address on shift registers, with output enabled on the chip
    // its safe to set output enabled because data_reg is in input mode
    shift_reg.shift_out(address & 0b11111111111);

    Packet {
        id,
        data: Data::Response(Response::Read {
            data: data_reg.read(),
        }),
    }
}

fn process_write(
    id: u8,
    address: u16,
    value: u8,
    shift_reg: &mut ShiftRegister<PD2, PD3, PD4>,
    data_reg: DataRegister<Input<Floating>>,
    write_enable: &mut Pin<Output, PB5>,
) -> (DataRegister<Input<Floating>>, Packet) {
    // output 11-bit address on shift registers, with output disabled on the chip
    shift_reg.shift_out((address & 0b11111111111) | 0x8000);

    arduino_hal::delay_ms(1);

    // switch data register to output, the output on the chip is disabled now
    let mut data_reg = data_reg.into_output();
    data_reg.write(value);

    write_enable.set_low();
    arduino_hal::delay_us(1);
    write_enable.set_high();

    // switch data register to input mode, enable chip output
    let data_reg = data_reg.into_floating_input();
    arduino_hal::delay_ms(1);
    shift_reg.shift_out(address & 0b11111111111);

    (
        data_reg,
        Packet {
            id,
            data: Data::Response(Response::Write),
        },
    )
}

struct DataRegister<Mode> {
    pins: [Pin<Mode>; 8],
}

impl DataRegister<Output> {
    fn into_floating_input(self) -> DataRegister<Input<Floating>> {
        DataRegister {
            pins: self.pins.map(|x| x.into_floating_input()),
        }
    }

    fn write(&mut self, value: u8) {
        for (i, pin) in self.pins.iter_mut().enumerate() {
            let state = value & (0x1 << i) != 0x00;
            if state {
                pin.set_high();
            } else {
                pin.set_low();
            }
        }
    }
}

impl DataRegister<Input<Floating>> {
    fn new(pins: [Pin<Input<Floating>>; 8]) -> Self {
        Self { pins }
    }

    fn into_output(self) -> DataRegister<Output> {
        DataRegister {
            pins: self.pins.map(|x| x.into_output()),
        }
    }

    fn read(&self) -> u8 {
        let mut value = 0x00u8;
        for (i, pin) in self.pins.iter().enumerate() {
            if pin.is_high() {
                value = value | (0x1 << i);
            }
        }
        value
    }
}

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);

    let mut serial = arduino_hal::default_serial!(dp, pins, 115200);

    // signal that the programmer has started
    serial.write_packet(&startup_packet());

    let mut shift_reg = ShiftRegister::new(
        pins.d2.into_output(),
        pins.d3.into_output(),
        pins.d4.into_output(),
    );

    let mut data_reg = DataRegister::new([
        pins.d5.downgrade(),
        pins.d6.downgrade(),
        pins.d7.downgrade(),
        pins.d8.downgrade(),
        pins.d9.downgrade(),
        pins.d10.downgrade(),
        pins.d11.downgrade(),
        pins.d12.downgrade(),
    ]);

    let mut write_enable = pins.d13.into_output_high();

    loop {
        let response = match serial.try_read_packet() {
            Ok(Some(Packet {
                id,
                data: Data::Command(Command::Ping(value)),
            })) => Some(process_ping(id, value)),
            Ok(Some(Packet {
                id,
                data: Data::Command(Command::Read { address }),
            })) => Some(process_read(id, address, &mut shift_reg, &data_reg)),
            Ok(Some(Packet {
                id,
                data: Data::Command(Command::Write { address, data }),
            })) => {
                let (dr, packet) = process_write(
                    id,
                    address,
                    data,
                    &mut shift_reg,
                    data_reg,
                    &mut write_enable,
                );
                data_reg = dr;
                Some(packet)
            }
            _ => None,
        };

        if let Some(packet) = response {
            serial.write_packet(&packet);
        }
    }
}
