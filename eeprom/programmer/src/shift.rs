use arduino_hal::port::{mode::Output, Pin};
use avr_hal_generic::port::PinOps;

pub struct ShiftRegister<Data, Clock, Latch> {
    data: Pin<Output, Data>,
    clock: Pin<Output, Clock>,
    latch: Pin<Output, Latch>,
}

fn pulse_pin<P>(pin: &mut Pin<Output, P>)
where
    P: PinOps,
{
    // create rising edge
    pin.set_high();

    // set to default state
    pin.set_low();
}

/// Shifts out value such that the LSB is the last shifted out.
impl<Data, Clock, Latch> ShiftRegister<Data, Clock, Latch>
where
    Data: PinOps,
    Clock: PinOps,
    Latch: PinOps,
{
    pub fn new(
        data: Pin<Output, Data>,
        mut clock: Pin<Output, Clock>,
        mut latch: Pin<Output, Latch>,
    ) -> Self {
        // set to default state
        clock.set_low();
        latch.set_low();

        Self { data, clock, latch }
    }

    pub fn shift_out(&mut self, value: u16) {
        for i in (0u8..16).rev() {
            // set data pin to correct state for bit
            let state = value & (0x1 << i) != 0x0000;
            if state {
                self.data.set_high();
            } else {
                self.data.set_low();
            }

            // clock the shift
            pulse_pin(&mut self.clock);
        }

        // latch the data to the output
        pulse_pin(&mut self.latch);
    }
}
